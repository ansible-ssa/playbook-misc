# Simple Apache Demo

Yet another simple demo to get started with Ansible.

## Inventory

Create a `hosts` file in the local directory and add a server to your inventory. For example:

```ini
[demo]
my-demo-server.example.com
```

**NOTE:** The server has to be manually created and SSH access with sudo is already configured. For this demo, this is out of scope. Verify everything works by running the following commands:

```bash
ssh my-demo-server.example.com # should log into the machine without asking for a password
sudo -i # should switch to root without asking for password
whoami # should show root
```

The playbooks have been tested with RHEL 8, but should work on other Linux distributions as well.

## Simple Apache Playbook

### Explain inventory

Show `inventory.yml` and explain groups and nested groups. Optionally mention YAML format.

### Use ansible-playbook

Run the playbook with `ansible-playbook simple-apache.yml`.

### Use Ansible Navigator

Run the same playbook after setting up [Ansible content navigator](https://github.com/ansible/ansible-navigator) with `ansible-navigator run simple-apache.yml`.

### Create multiple users

Use the `create-users.yml` playbook to talk about loops.

### Roles

To demonstrate Ansible encourages reusable code, use the `system-roles.yml` example, which uses the `redhat.rhel_system_roles` collection. Make sure it's installed on your local system by running `ansible-galaxy collection install redhat.rhel_system_roles`.

### Site

Finally use the `site.yml` example to show how we can combine multiple playbooks.

## Cleanup

To be able to run the same playbooks multiple times on the same demo instance, the `simple-apache-cleanup.yml` and `remove-users.yml` playbooks will revert the previous changes.
